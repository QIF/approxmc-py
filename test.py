#!/usr/bin/python3

from unittest import *
from approxmc.algorithm import *

def run_approxmc_p(filename):
    ss = SharpSatCLI("sharpSat {file}", None, "/tmp")
    a = ApproxMC(file = filename,
                 epsilon = 0.1,
                 delta = 0.9,
                 lower_bound = None,
                 sharp_solver = ss);
    count = a.count()

def test_all_files():
    for f in glob("test/*.cnf"):
        yield run_approxmc_p, f, 2**32
