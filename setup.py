from distutils.core import setup

setup(
    name='approxmc-py',
    version='4',
    packages=[''],
    url='',
    license='GPLv3.0',
    author='Alexander Weigl',
    author_email='weigl@kit.edu',
    description='ApproxMC for Python', 
    install_requires=['scipy'],
     
)
