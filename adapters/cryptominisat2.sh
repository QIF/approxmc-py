#!/bin/bash -e

function error() { echo "$@" 1>&2; }

CMD=cryptominisat2

if ! which $CMD; then
    error "$CMD is not in path"
    exit 124
fi

error "Using $CMD from: " $(which $CMD)

MC="$1"
FILE="$2"

$CMD --nosolprint --verbosity=0 --maxsolutions="$MC" "$FILE" | grep "c SATISFIABLE" | wc -l
