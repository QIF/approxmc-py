#!/usr/bin/env bash

function error() { echo "$@" 1>&2; }

if ! which xcnf2cnf; then
    error "xcnf2cnf is not in path"
    error "download from gitlab »git clone https://github.com/..../xor«"
    exit 124
fi

error "Using xcnf2cnf from: " $(which xcnf2cnf)

FILE=$1
OUTPUT="/tmp/$(basename $FILE).blasted.cnf"
xcnf2cnf <$FILE >$OUTPUT
echo $OUTPUT
