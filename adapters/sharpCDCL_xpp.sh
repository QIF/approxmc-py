#!/bin/bash

function error() { echo "$@" 1>&2; }

CMD=sharpCDCL

if ! which $CMD; then
    error "$CMD is not in path"
    error "download from gitlab »git clone git@gitlab.com:QIF/sharpCDCL.git«"
    exit 124
fi

error "Using $CMD from: " $(which $CMD)

MC=$1
FILE=$2

# files
BLASTFILE=${FILE}.blasted
RFILE=/tmp/$(basename $FILE).rem
LOGFILE=/tmp/$(basename $FILE).log

xorblastpp -g -o $BLASTFILE  $FILE

grep '^cr .*' ${FILE} | cut -c 4- | tr ' ' "\n" > $RFILE

$CMD -countMode=2 -maxModel="${MC}" -projection="$RFILE" "$BLASTFILE" > "$LOGFILE"


exitcode=$?
if [ $exitcode -eq 20 ]; then
  echo 0
  exit 0
elif [ $exitcode -eq 0 ]; then
  tail -n 1 "$LOGFILE"
elif [ $exitcode -eq 139 ]; then
  echo 0
  exit 0
else
 exit $exitcode
fi


