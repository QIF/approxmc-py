#!/usr/bin/env bash

function error() { echo "$@" 1>&2; }

if ! which xorblast.py; then
    error "xorblast.py is not in path"
    error "download from gitlab »git clone git@gitlab.com:QIF/xorblast.git«"
    exit 124
fi

error "Using xorblast.py from: " $(which xorblast.py)


FILE=$1
OUTPUT="/tmp/$(basename $FILE).blasted.cnf"
xorblast.py --no-gauss $FILE > $OUTPUT
echo $OUTPUT
