#!/bin/bash -e

function error() { echo "$@" 1>&2; }

CMD=cryptominisat4

if ! which $CMD; then
    error "$CMD is not in path"
    exit 124
fi

error "Using $CMD from: " $(which $CMD)

MC=$1
FILE=$2

$CMD --printsol=0 --verb=0 --autodisablegauss=0 --maxsol="$MC" "$FILE" | grep "s SATISFIABLE" | wc -l
