#!/bin/bash

function error() { echo "$@" 1>&2; }

CMD=clasp

if ! which $CMD; then
    error "$CMD is not in path"
    exit 124
fi

error "Using $CMD from: " $(which $CMD)

MC=$1
FILE=$2

# files
BLASTFILE=${FILE}.blasted
RFILE=/tmp/$(basename $FILE).rem
LOGFILE=/tmp/$(basename $FILE).log

xorblastpp -g -o $BLASTFILE  $FILE

grep '^cr .*' ${FILE} | cut -c 4- | tr ' ' "\n" > $RFILE

$CMD -q --project -n "${MC}" "$BLASTFILE" > "$LOGFILE"


exitcode=$?
if [ $exitcode -eq 139 ]; then
  echo 0
  exit 0
else
  grep -oP "c Models        : \K[0-9]+" "$LOGFILE"
  exit 0
fi


