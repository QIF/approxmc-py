#!/bin/bash

function error() { echo "$@" 1>&2; }

MAX="$1"
FILE="$2"

CNF2LP=/home/i57/klebanov/sharpclasp/src/cnf2lp.py
GRINGO=/home/i57/klebanov/gringo-3.0.5-source/build/bin/gringo
#PROJ=$(grep "^cr" "$1" | cut -c 4- | tr " " ",")
CLASP=/home/i57/klebanov/sharpclasp/src/build/release/bin/clasp

# files
BLASTFILE=$(xortseitin.sh $FILE | tail -n 1)
RFILE=/tmp/$(basename $FILE).rem
LOGFILE=/tmp/$(basename $FILE).log



#$CNF2LP "$BLASTFILE" "$PROJ" > "$CNF.lp"
$CNF2LP "$BLASTFILE" > "$BLASTFILE.lp"

$GRINGO "$BLASTFILE.lp" > "$BLASTFILE.clasp"
$CLASP -n $MAX --project -q "$BLASTFILE.clasp" | grep -oP "^Models       : \K[0-9]+"
