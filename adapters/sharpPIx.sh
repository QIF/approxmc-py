#!/bin/bash

function error() { echo "$@" 1>&2; }

CMD=${SHARPPI:-sharpPI}

if ! which $CMD; then
    error "$CMD is not in path"
    exit 124
fi

error "Using $CMD from: " $(which $CMD)

MC=$1
FILE=$2

# files
BLASTFILE=$(xortseitin.sh $FILE | tail -n 1)
LOGFILE=/tmp/$(basename $FILE).log

$CMD -m 5 -v -n $1 $BLASTFILE > "$LOGFILE"
e=$?
tail -n 1 "$LOGFILE" | cut -d' ' -f 3
exit $?
