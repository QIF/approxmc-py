# This file is part of approxmc-py

# approxmc-py is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# approxmc-py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with approxmc-py. If not, see <http://www.gnu.org/licenses/>.

"""
This module encapsulate the SAT solver interfaces.
In general you want to use the `SharpSatCLI` class.
"""

import abc
import logging
import tempfile
from   io         import StringIO
from   subprocess import Popen, PIPE, TimeoutExpired
from   .cnf       import CNFFile

logger = logging.getLogger(__name__)


class SharpSat(object):
    """Abstract class describing the interface for calling #SAT programs.
    """

    def __init__(self, timeout=None, temp_dir=None):
        """ Construct a SharpSAT instance. You need to overload the `__call__()` operator
        :param timeout: an float value (seconds) describing the timeout of #SAT call. `None` if no timeout is wanted.
        :param temp_dir: the temporary directory for putting DIMACS files. `None` for default folder defined by :py:mod`tempfile.gettempdir()`
        """
        self._timeout = timeout

        self._temp_dir = temp_dir if temp_dir else tempfile.gettempdir()

    def __call__(self, file: CNFFile, constraints=None, max_count=1):
        return self.count(file, constraints, max_count)

    @abc.abstractmethod
    def count(self, file: CNFFile,
              constraints=None,
              max_count=1):
        """Interface for calling a #SAT.

        :param file: a CNFFile instance
        :param constraints: a list of xor clauses
        :param max_count: maximal count of models wanted
        :raise AssertError: if something went wrong ($? != 0)
        :return: the number of found models or None if timeout exceeded
        :rtype: int
        """
        pass


class SharpSatCLI(SharpSat):
    def __init__(self,
                 command,
                 timeout=None,
                 temp_dir=None):
        """
        :param command: a format string
        :param timeout: see SharpSAT.__init__
        :param temp_dir: see SharpSAT.__init__
        :return:
        """
        super(SharpSatCLI, self).__init__(timeout, temp_dir)
        self._command = command
        self.cnt = 0

    def count(self, file: CNFFile, constraints=None, max_count=1):
        # create temprary filename
        filename = tempfile.mktemp(".b%03d.cnf" % self.cnt, "", self._temp_dir)
        self.cnt += 1

        # fill in the values for the command
        cmd = self._command.format(maxcount=max_count, file=filename)

        # if command takes a file argument
        # then create a temporary file with the cnf and xor clauses as input
        if "{file}" in self._command:
            with open(filename, 'w') as fh:
                file.copy_into(fh)
                xor_constraint_as_xcnf(constraints, filehandle=fh)
            logger.debug("create input file %s" % filename)

        logger.info("execute: %s" % cmd)

        # execute in a shell environment, capture everything
        process = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

        # if the program does not take an file.
        # then we serve the cnf via the stdin.
        if not "{file}" in self._command:
            file.copy_into(process.stdin)
            xor_constraint_as_xcnf(constraints, filehandle=process.stdin)

            # closing the stream can be imported to some program
            process.stdin.close()

        lines = []
        try:
            # wait for termination, if timeout exceeded we catch
            process.wait(self._timeout)

            # some error happened
            if process.returncode > 0:
                raise AssertionError("SAT Call results in error. See log for more information")

            # get last line
            lines = process.stdout.readlines()
            # parse last line as int (model count)
            return int(lines[-1].strip())
        except TimeoutExpired as e:
            # kill the process, (hard)
            process.kill()
            return None
        finally:
            if logger.getEffectiveLevel() == logging.DEBUG:
                for l in lines:
                    logger.debug("SAT: %s", l)

                for l in process.stderr.readlines():
                    logger.debug("SAT (err): %s", l)


def xor_constraint_as_xcnf(constraints, filehandle=None):
    """xor clauses to string.

    if `filehandle` is given, then write the result to the filehandle,
    else a string is returned.


    >>> print(xor_constraint_as_xcnf([ [-1, 2, 3, 4], [6,8], [9,2]]))
    <BLANKLINE>
    x -1 2 3 4 0
    x 6 8 0
    x 9 2 0
    <BLANKLINE>

    :param constraints: a list of list of integers (literals)
    :param filehandle: `file`-like object
    :return: str | NoneType
    """

    if constraints is None:
        return

    sio = False
    if not filehandle:
        filehandle = StringIO()
        sio = True

    # to be sure, could result in warnings from SAT solvers
    filehandle.write("\n")

    for cs in constraints:
        filehandle.write("x " + ' '.join(map(str, cs)) + " 0\n")

    if sio:
        return filehandle.getvalue()
