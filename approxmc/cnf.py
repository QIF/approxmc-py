# This file is part of approxmc-py

# approxmc-py is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# approxmc-py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with approxmc-py. If not, see <http://www.gnu.org/licenses/>.

"""

"""

import shutil

__author__ = "Alexander Weigl <weigl@kit.edu>"
__date__ = "2015-11-06"


class CNFFile(object):
    """Representation of DIMACS files.
    Support for reading projection corpus (cr directive) and number of variables and clauses.
    """
    def __init__(self, filename):
        self._filename = filename
        self._projection_scope = None
        self.nvars = 0
        self.nclauses = 0
        self._read_in()

    def _read_in(self):
        with open(self._filename) as fh:
            # parse first line
            p, cnf, self.nvars, self.nclauses = fh.readline().split(" ")

            self.nvars = int(self.nvars)
            self.nclauses = int(self.nclauses.strip())

            self._projection_scope = []

            # search for the projection corpus
            for line in fh.readlines():
                if line[0] == 'c' and line[1] == 'r':
                    self._projection_scope += parse_int(line[2:])

                    # assume there is only one cr statement
                    break

    def copy_into(self, fd):
        """copies the contents of the cnffile into the file descriptor.

        :param fd: file-like object
        :return: None
        """
        with open(self._filename) as fh:
            shutil.copyfileobj(fh, fd)

    @property
    def projection_scope(self):
        if self._projection_scope:
            return self._projection_scope
        else:
            return range(1, self.nvars+1)
    @property
    def filename(self):
        return self._filename


def parse_int(string):
    """Parse a string to an list of integers,
    :rtype: list[int]
    """
    return map(int, string.strip().split(" "))
