# This file is part of approxmc-py

# approxmc-py is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# approxmc-py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with approxmc-py. If not, see <http://www.gnu.org/licenses/>.

import logging
import random
import sys

# prepare logging, should be the first thing we do
rootlogger = logging.getLogger("approxmc")
sh = logging.StreamHandler(stream=sys.stderr)
sh.setFormatter(
    logging.Formatter(
        "%(relativeCreated)10d:%(name)-24s:%(levelname)5s> %(message)s"))
rootlogger.addHandler(sh)
##

logger = logging.getLogger(__name__)

import argparse, os
import approxmc
from .algorithm import ApproxMC
from .cnf       import CNFFile
from .sat       import SharpSatCLI


def get_argument_parser():
    """Creates an :py:class:`argparse.ArgumentParser` for approxmc-py

    :return:
    """
    a = argparse.ArgumentParser()

    a.add_argument("-c", "--confidence",
                   help="confidence (1 - δ) for the approximation",
                   type=float,
                   default=0.85)

    a.add_argument("-t", "--tolerance",
                   default=0.5,
                   type=float,
                   help="tolerance (ε) of the approximatio")

    a.add_argument("--lower_bound",
                   default=0,
                   type=int,
                   help="lower bound on the number of model")

    a.add_argument("--timeout",
                   default=None,
                   type=int,
                   help="timeout in seconds for an approximation")

    a.add_argument("--seed", help="set the seed for the internal PRNG",
                   default=None, action="store", metavar="INT")

    a.add_argument("-v", "--verbose", action="count",
                   help="increase output verbosity")

    a.add_argument("-p", "--parallel-sat",
                   metavar="INT",
                   help="parallel calls of sat solver")

    a.add_argument("--version", help="print version information", action="store_true")

    # (Q)SAT OPTIONS

    a.add_argument("--sat-command",
                   default=os.environ.get('SAT_SOLVER_COMMAND'),
                   help="command for invoking #SAT (must be absolute path)")

    a.add_argument("--threads",
                   help="number of threads per cryptominisat2 instance",
                   type=int)

    a.add_argument("--sat-timeout",
                   default=None,
                   help="timeout in seconds for cryptominisat2 calls (currently not supported)",
                   action="store")

    a.add_argument("--request-dir", help="directory to save the requests to the solver")

    a.add_argument("file", metavar="FILE", default=None, nargs='?')
    return a


BANNER="""
    _                              __  __  ____
   / \   _ __  _ __  _ __ _____  _|  \/  |/ ___|     _ __
  / _ \ | '_ \| '_ \| '__/ _ \ \/ / |\/| | |   _____| '_ \
 / ___ \| |_) | |_) | | | (_) >  <| |  | | |__|_____| |_) |
/_/   \_\ .__/| .__/|_|  \___/_/\_\_|  |_|\____|    | .__/
        |_|   |_|                                   |_|
"""


def print_version():
    print("""ApproxMC-p
Version:   %s
Copyright: Jörg Weisbarth, Alexander Weigl, Vladimir Klebanov
           Karlsruhe Institute for Technology,
           Institute for Theoretical Informatics,
           Application-oriented Formal-Verification, Prof. Beckert

For more information:
           https://gitlab.com/QIF/approxmc-py
""" % approxmc.__version__)


def main():
    """Main-Entry point.

    :return:
    """
    ap = get_argument_parser()
    ns = ap.parse_args()

    if ns.version:
        print_version()
        return 0

    random.seed(ns.seed)

    if ns.verbose:
        V = {0: "ERROR", 1: "WARN", 2: "INFO", 3: "DEBUG"}
        if ns.verbose in V:
            logging.getLogger("approxmc").setLevel(V[ns.verbose])
        elif ns.verbose >= 3:
            logging.getLogger("approxmc").setLevel("DEBUG")

    sharp_sat = SharpSatCLI(ns.sat_command, ns.sat_timeout, ns.request_dir)

    logger.info("start")
    logger.info("epsilon:     %s", ns.tolerance)
    logger.info("delta:       %s", ns.confidence)
    logger.info("SAT command: %s", ns.sat_command)
    #logger.info("SAT timeout: %s", ns.sat_timeout)

    cnffile = CNFFile(ns.file)
    algo = ApproxMC(cnffile,
                    epsilon=ns.tolerance,
                    delta=1 - ns.confidence,
                    sharp_solver=sharp_sat,
                    lower_bound=ns.lower_bound)
    c = algo.count()
    print("Model-Count: %d" % c)
