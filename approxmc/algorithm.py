# This file is part of approxmc-py

# approxmc-py is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# approxmc-py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with approxmc-py. If not, see <http://www.gnu.org/licenses/>.

"""ApproxMC Algorithm.
"""

import itertools
import logging
import logging.handlers
import random
from math import *

from scipy import median
from scipy.special import binom as binomial

from approxmc.cnf import CNFFile
from approxmc.sat import SharpSat

logger = logging.getLogger(__name__)


# logger.addHandler(logging.handler)

class ApproxMC(object):
    """Implementation of an (ε, δ)-counter for propositional models.
    """

    def __init__(self, file: CNFFile, epsilon: float, delta: float, lower_bound: int, sharp_solver: SharpSat):
        """
        :param file:
        :param epsilon:
        :param delta:

        """

        assert( 0 <= epsilon < 1)
        assert( 0 <= delta < 1)

        self._file = file
        self._epsilon = epsilon
        self._delta = delta
        self._lower_bound = lower_bound

        self._sharp_sat = sharp_solver

    def count(self):
        """(ε, δ) approximation on the number of models of the propositional cnf

            If the computation times out then 'Nothing' is returned, otherwise the result.
            To be able to run this function requires to have the program 'cryptominisat2' available in
            one of the PATH directories.

            ApproxMC implements an (epsilon, delta)-counter on the number of models for a propositional
            formula in conjunctive normal form.
        """

        epsilon = self._epsilon
        delta = self._delta
        pivot = min_pivot_size(epsilon)
        t = min_num_samples(delta)
        logger.debug("approxMC: pivot = %d" % pivot)

        # if the lower bound is bigger than the needed
        # bound, we just say that there are more elements than pivot,
        # to trigger the approx count.
        if self._lower_bound > pivot:
            count = pivot + 1
        else:
            count = self._sharp_sat(
                file=self._file,
                max_count=pivot + 1)

        if count <= pivot:
            logger.debug("approxMC: returning exact solution")
            return count
        else:
            logger.debug("approximative solution doing t = %s repetitions", t)

            #pivot = max( pivot, self._lower_bound)

            # do the experiment t times:
            samples = [self.__approx_mc_sample(pivot) for i in range(t)]
            logger.debug("approxMC: returning median of %d", len(samples))
            return median(samples)

    def __approx_mc_sample(self, pivot):
        # here we need to shuffle right, for producing uniformly distributed hash functions
        # ah... forget it!

        nvars = self._file.nvars

        min_drawers = log2_min_num_drawers(self._epsilon, self._lower_bound)
        lower_m = max(1, min_drawers)
        upper_m = nvars + 1

        m = lower_m - 1  # (m) loop variable, see paper
        c = pivot + 1  # results of 'boundedSAT'

        # finish looping when 'm' is too big or when a drawer ('c') is sufficiently small
        while not (c <= pivot or m > upper_m):
            m += 1

            logger.debug("\t\tapproxMCSample: starting run with 2 ^ m = 2 ^ %d = %d;" % (m, 2 ** m))

            xor_clauses = with_hash_constraint(m, self._file.projection_scope)

            # compute and adapt new value for 'count'
            count = self._sharp_sat(
                file=self._file,
                constraints=xor_clauses,
                max_count=pivot + 1
            )

            if count is not None:
                c = count
            else:  # repeat cycle, when timed out
                m -= 1
                logger.debug("timed out; decrementing m to %d", m)

        logger.debug("terminated run with m = %s and c = %s.",  m, c)
        return 2 ** m * c


def min_pivot_size(epsilon: float):
    """Calculates the minimum size of the pivot element in the paper.

    >>> min_pivot_size(0.001)
    8382048
    >>> min_pivot_size(0.3)
    120
    >>> min_pivot_size(0.4)
    73
    >>> min_pivot_size(0.5)
    50
    >>> min_pivot_size(0.8)
    23
    >>> min_pivot_size(0.9)
    19
    >>> min_pivot_size(1)
    16

    :param epsilon: the error tolerance
    :return:
    """

    if epsilon <= 0:
        raise AssertionError("minPivotSize: tolerance must be greater than 0")

    eps = epsilon  # calculate as much as possible with Rational for higher precision
    e13 = exp(1.0 / 3.0)
    num = 6 * (1 + eps) * e13 / eps ** 2  # -- parameter r = 3
    return ceil(num) - 1


def min_num_samples(delta: float):
    """Calculates the minimal number of samples of the random experiment
     to achieve the wished confidence

    >>> min_num_samples(0.000000001)
    47
    >>> min_num_samples(0.00001)
    25
    >>> min_num_samples(0.001)
    13
    >>> min_num_samples(0.01)
    7
    >>> min_num_samples(0.05)
    3
    >>> min_num_samples(0.09)
    3
    >>> min_num_samples(0.1)
    3
    >>> min_num_samples(0.2)
    1
    >>> min_num_samples(0.3)
    1
    >>> min_num_samples(0.4)
    1
    >>> min_num_samples(0.5)
    1

    :param delta: the inverse confidence
    :return:
     """

    if delta <= 0 or delta >= 1:
        raise AssertionError("minNumSamples: number of required samples for confidence 1 is infinity")

    # -- probability of a ApproxMCSample call to fail guarantees
    p = exp(floor(-3 / 2))  # -- parameter r = 3

    for n in itertools.count(start=1):
        if (eta(n, ceil(n / 2), p)) <= delta:
            return n


def eta(coin_tosses: int, border: int, prob_head: float):
    """Probability to achieve with `coin_tosses`, and `prob_head` at least `border` times head

    Binomial Distribution:

    * P(X = k) =   ` ((n over k)) p^k (1-p)^{n-k}`

    * eta == P(X >= border) where X ~ B(coin_tosses, prob_head)

    >>> eta(2, 1, 0.5) # toss two times, want at least on head,
    0.75
    >>> eta(2, 1, 0.25) # toss two times, want at least on head,
    0.4375
    >>> eta(2, 1, 1) # toss two times, want at least on head,
    1.0
    >>> eta(2, 2, 0.5) # toss two times, want at least on head,
    0.25
    >>> eta(3, 2, 0.5) # toss two times, want at least on head,
    0.5
    >>> eta(4, 2, 0.5) # toss two times, want at least on head,
    0.6875
    >>> eta(5, 2, 0.5) # toss two times, want at least on head,
    0.8125
    >>> eta(10, 10, 0.5) # toss two times, want at least on head,
    0.0009765625

    :param coin_tosses: how often is a the coin toss
    :param border:
    :param p:
    :return:
    """

    def cointoss(n, k):
        return binomial(coin_tosses, k) * \
               (prob_head ** k) * \
               (1 - prob_head) ** (coin_tosses - k)

    gen = (cointoss(coin_tosses, k) for k in range(border, coin_tosses + 1))
    return sum(gen)


def log2_min_num_drawers(epsilon, lower_bound):
    """Logarithmus of the minimum number of drawers which ApproxMCSample has to use

    Given the tolerance 'epsilon' and a 'lowerBound' on the number of models of a formula this
    functions returns the logarithmus dualis of a minimum number of drawers where ApproxMCSample can
    start from without losing any guarantees.

    >>> log2_min_num_drawers(0.5, 20)
    0
    >>> log2_min_num_drawers(0.75, 20)
    1
    >>> log2_min_num_drawers(1.0, 20)
    2
    >>> log2_min_num_drawers(0.5, 42)
    1
    >>> log2_min_num_drawers(0.75,42)
    2
    >>> log2_min_num_drawers(1.0, 42)
    3

    :param epsilon:
    :param lower_bound:
    """

    piv = float(min_pivot_size(epsilon))
    lower_bound = float(lower_bound)

    tmp = (1 + epsilon) * lower_bound / piv

    if tmp == 0:  # error case not in haskell
        return 0
    else:
        return ceil(log2((1 + epsilon) * lower_bound / piv))


def log2_max_num_skipable_drawers(epsilon, lower_bound):
    """Logarithmus of the maximum number of drawers for which ApproxMCSample has a low return-probability

    Given the tolerance 'epsilon' and a 'lowerBound' on the number of models of a formula this
    functions returns the logarithmus dualis of a maximum number of drawers where ApproxMCSample is
    unlike to return.

    >>> log2_max_num_skipable_drawers(0.5, 20)
    -1
    >>> log2_max_num_skipable_drawers(0.8, 20)
    0
    >>> log2_max_num_skipable_drawers(0.9, 20)
    0
    >>> log2_max_num_skipable_drawers(1.0, 20)
    1
    >>> log2_max_num_skipable_drawers(0.8, 10)
    -1
    >>> log2_max_num_skipable_drawers(0.8, 20)
    0
    >>> log2_max_num_skipable_drawers(0.8, 40)
    1
    >>> log2_max_num_skipable_drawers(0.8, 50)
    1
    >>> log2_max_num_skipable_drawers(0.8, 60)
    2


    :param epsilon: the tolerance
    :type epsilon: float
    :param lower_bound: lower_bound of the drawers
    :type lower_bound: int

    :return:
    :rtype: int
    """

    piv = float(min_pivot_size(epsilon))
    lower_bound = float(lower_bound)

    t = log2((1 + epsilon) * lower_bound / piv)
    m = floor(t)

    if m == t:
        # assert(m < log_2((1 - ε) * lowerBound / pivot), that truly it holds: )
        return m - 1
    else:
        return m


def with_hash_constraint(number_of_constraints: int, variables: list):
    """Creates a new xor constraints (aka hash constraint) by selecting random variables from the projection corpus.

    >>> len(list(with_hash_constraint(5, [1,2,3,4])))
    5

    returns three generator objects
    >>> with_hash_constraint(3, []) # doctest:+ELLIPSIS
    <generator ...>
    >>> list(with_hash_constraint(0, [1,2,3,4]))
    []

    :param number_of_constraints: count of constriants wanted
    :param variables: list of variables, considered for the hash constraint

    :return: the new xor clauses, as generator of generators
    :rtype: list[list[int]]

    Facts:

        - we need to create `m` xor contraints.
        - Each xor constraint is randomize
          - We need to shuffle over the variables
          - We need to shuffle over the parity of the constraint


    """
    return (filter_by_random(variables) for i in range(number_of_constraints))


def filter_by_random(seq: list):
    """Select a random items from the sequence.

    In 0.5 of the cases, the first item is negative, meaning the xor constraint checks for ... = 0

    >>> list(filter_by_random([1,2,3])) # doctest:+ELLIPSIS
    [...]

    :param seq: a list of variables
    :return:
    """
    signum = random.getrandbits(1)
    mask = random.getrandbits(len(seq))

    gen = filter_by_mask(seq, mask)

    if signum:  # first item is negative
        yield -1 * next(gen)

    yield from gen


def filter_by_mask(seq: list, mask: int):
    """Filter the given `seq` by the bits in the given `mask`.
    If bit at n-th position is 1, then seq[n] is yielded.

    >>> list( filter_by_mask([1,2,3,4,5], 0) )
    []
    >>> list( filter_by_mask([1,2,3,4,5], 1) )
    [1]
    >>> list( filter_by_mask([1,2,3,4,5], 2) )
    [2]
    >>> list( filter_by_mask([1,2,3,4,5], 8) )
    [4]
    >>> list( filter_by_mask([1,2,3,4,5], 2+4) )
    [2, 3]
    >>> list( filter_by_mask([1,2,3,4,5], 2+4+8) )
    [2, 3, 4]
    >>> list( filter_by_mask([1,2,3,4,5], 63) )
    [1, 2, 3, 4, 5]

    :param seq:
    :param mask:
    :return:
    """

    for var in seq:
        # if var was chosen by random
        if mask & 1 > 0:
            yield var

        # next random variable
        mask >>= 1


def bits_of(num, sz=None):
    """Representation of a Word8 as a list of eight boolean values.


    >>> bits_of(0, sz = 8)
    [False, False, False, False, False, False, False, False]
    >>> bits_of(7, sz = 8)
    [False, False, False, False, False, True, True, True]
    >>> bits_of(255)
    [True, True, True, True, True, True, True, True]

    :param num:
    :param sz:
    """

    if not sz:
        sz = ceil(log2(num))

    seq = [num & (2 ** i) > 0 for i in range(sz)]
    seq.reverse()
    return seq
