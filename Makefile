gendoc:
#	wget -O style.css https://raw.githubusercontent.com/sindresorhus/github-markdown-css/gh-pages/github-markdown.css
	rst2html.py --cloak-email-addresses --link-stylesheet\
		--no-generator --time --date --section-numbering\
		--stylesheet=style.css\
		README.rst index.html

publish: gendoc
	scp index.html i57pc7.ira.uka.de:htdocs/weigl/software/approxmc-py/

test:
	python doctestrunner.py
	python test.py
