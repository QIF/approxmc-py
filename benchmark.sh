#!/bin/bash

declare -a solvers=("cryptominisat4.sh" "sharpCDCL.sh" "clasp.sh" "sharpclasp.sh")
SEED=$(od -vAn -N8 -tu8 /dev/urandom)

do_one_command() {
    local ADAPTER="$1"
    local ARGS="${@:2}"
    echo "--- $ADAPTER:  $ARGS"
    ./run.py --sat-command="./adapters/$ADAPTER {maxcount} {file}" -vvv \
        --seed $SEED \
        $ARGS  |& grep -E "approxmc.algorithm|^Model"
    echo "END $ADAPTER:  $ARGS ----------"
}

do_one_problem() {
    local ARGS="$@"
    for i in "${solvers[@]}"
    do
        do_one_command "$i" "$ARGS"
    done
}

do_one_problem "$@"
