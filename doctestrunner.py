import doctest
import approxmc.algorithm
import approxmc.cli
import approxmc.cnf
import approxmc.sat

doctest.testmod(approxmc.algorithm)
doctest.testmod(approxmc.cli)
doctest.testmod(approxmc.cnf)
doctest.testmod(approxmc.sat)
